alien_material = {}

-- dofile
dofile(minetest.get_modpath("alien_material") .. "/ores.lua")
dofile(minetest.get_modpath("alien_material") .. "/tools.lua")
dofile(minetest.get_modpath("alien_material") .. "/craft.lua")
if minetest.get_modpath("3d_armor") then
	dofile(minetest.get_modpath("alien_material") .. "/armor.lua")
end
if minetest.get_modpath("mobs") then
	dofile(minetest.get_modpath("alien_material") .. "/alien.lua")
end

-- Alien Diamond

minetest.register_node("alien_material:alien_diamond_block", {
		tiles = {
				"alien_diamond_block.png"
		},
		groups = {cracky = 1},
		drop = "alien_material:alien_diamond_block",
		description = "Alien Diamond Block",
		light_source = 15
})

minetest.register_node("alien_material:alien_diamond_ore", {
		tiles = {
				"default_stone.png^alien_diamond_ore.png"
		},
		groups = {cracky = 1},
		drop = "alien_material:alien_diamond",
		description = "Alien Diamond Ore",
		is_ground_content = true,
		legacy_mineral = true,
		light_source = 5
})

minetest.register_craftitem("alien_material:alien_diamond", {
		inventory_image = "alien_diamond.png",
		description = "Alien Diamond",
})

-- Alien Mese

minetest.register_node("alien_material:alien_mese_ore", {
description = "Alien Mese Ore",
tiles = {
"default_stone.png^alien_mese_ore.png"
},
groups = {cracky = 1},
is_ground_content = true,
legacy_mineral = true,
light_source = 5,
drop = "alien_material:alien_mese",
})

minetest.register_node("alien_material:alien_mese_block", {
		tiles = {
				"alien_mese_block.png"
		},
		groups = {cracky = 1},
		is_ground_content = true,
		legacy_mineral = true,
		drop = "alien_material:alien_mese_block",
		description = "Alien Mese Block",
		light_source = 15
})

minetest.register_craftitem("alien_material:alien_mese", {
		inventory_image = "alien_mese.png",
		description = "Alien Mese",
})

minetest.register_craftitem("alien_material:alien_mese_fragment", {
		inventory_image = "alien_mese_fragment.png",
		description = "Alien Mese fragments",
})

--Alien Ore

minetest.register_craftitem("alien_material:alien_ingot", {
		inventory_image = "alien_ingot.png",
		description = "Alien Ingot",
})

minetest.register_node("alien_material:alien_block", {
		tiles = {
				"alien_block.png"
		},
		groups = {cracky = 1},
		drop = "alien_material:alien_block",
		description = "Alien Block",
		light_source = 35
})
