-- Tools

minetest.register_tool("alien_material:alien_pickaxe", {
    description = "Alien Pickaxe",
    inventory_image = "alien_pickaxe.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            cracky = {
                maxlevel = 5,
                uses = 65536,
                times = { [1]=1.00, [2]=0.50, [3]=0.20 }
            },
        },
        damage_groups = {cracky=2},
    },
})

minetest.register_tool("alien_material:alien_axe", {
    description = "Alien Axe",
    inventory_image = "alien_axe.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            choppy = {
                maxlevel = 5,
                uses = 65536,
                times = { [1]=1.00, [2]=0.50, [3]=0.20 }
            },
        },
        damage_groups = {choppy=2},
    },
})

minetest.register_tool("alien_material:alien_spade", {
    description = "Alien Spade",
    inventory_image = "alien_spade.png",
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            crumbly = {
                maxlevel = 5,
                uses = 65536,
                times = { [1]=1.00, [2]=0.50, [3]=0.20 }
            },
        },
        damage_groups = {crumbly=2},
    },
})

minetest.register_tool("alien_material:alien_sword", {
    description = "Alien Sword",
    inventory_image = "alien_sword.png",
    tool_capabilities = {
        full_punch_interval = 0,3,
        max_drop_level = 1,
        groupcaps = {
            fleshy = {
                maxlevel = 5,
                uses = 65536,
                times = { [1]=1.00, [2]=0.50, [3]=0.20 }
            },
        },
        damage_groups = {fleshy=80},
    },
})

