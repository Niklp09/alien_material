armor:register_armor("alien_material:alien_boots", {
	description = "Alien Boots",
	inventory_image = "armor_inv_boots_alien.png",
  groups = {armor_fire=4, armor_feet=4,
		armor_heal=6, armor_use=1,
		physics_jump= 0.8, physics_speed=1.5, physics_gravity=0},
	armor_groups = {fleshy=6, radiation=16384},
	damage_groups = {cracky=3, snappy=3, choppy=2, crumbly=2, level=1},
	texture = 'armor_boots_alien.png',
  preview = 'armor_boots_alien_preview.png',
})

armor:register_armor("alien_material:alien_helmet", {
	description = "Alien Helmet",
	inventory_image = "armor_inv_helmet_alien.png",
	groups = {armor_head=5, armor_heal=6.5, armor_use=1},
	armor_groups = {fleshy=6.5, radiation=16384},
	damage_groups = {cracky=3, snappy=3, choppy=2, crumbly=2, level=1},
	texture = 'armor_helmet_alien.png',
  preview = 'armor_helmet_alien_preview.png',
})

armor:register_armor("alien_material:alien_leggings", {
	description = "Alien Leggings",
	inventory_image = "armor_inv_leggings_alien.png",
	groups = {armor_legs=7, armor_heal=12.5, armor_use=1},
	armor_groups = {fleshy=12.5, radiation=8192},
	damage_groups = {cracky=3, snappy=3, choppy=2, crumbly=2, level=1},
	texture = 'armor_leggings_alien.png',
  preview = 'armor_leggings_alien_preview.png',
})

armor:register_armor("alien_material:alien_chestplate", {
	description = "Alien Chestplate",
	inventory_image = "armor_inv_chestplate_alien.png",
	groups = {armor_torso=8, armor_heal=14.5, armor_use=1},
	armor_groups = {fleshy=14.5, radiation=8192},
	damage_groups = {cracky=3, snappy=3, choppy=2, crumbly=2, level=1},
	texture = 'armor_chestplate_alien.png',
  preview = 'armor_chestplate_alien_preview.png',
})

armor:register_armor("alien_material:alien_shield", {
	description = "Alien Shield",
	inventory_image = "armor_inv_shield_alien.png",
	groups = {armor_shield=7, armor_heal=12.5, armor_use=1},
	armor_groups = {fleshy=12.5, radiation=16384},
	damage_groups = {cracky=3, snappy=3, choppy=2, crumbly=2, level=1},
	texture = 'armor_shield_alien.png',
  preview = 'armor_shield_alien_preview.png',
})
