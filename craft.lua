-- Craft

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_mese_fragment 9",
    recipe = {
        {"alien_material:alien_mese"},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_mese",
    recipe = {
        {"alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",},
				{"alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",},
				{"alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",  "alien_material:alien_mese_fragment",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_diamond_block",
    recipe = {
        {"alien_material:alien_diamond",  "alien_material:alien_diamond",  "alien_material:alien_diamond",},
				{"alien_material:alien_diamond",  "alien_material:alien_diamond",  "alien_material:alien_diamond",},
				{"alien_material:alien_diamond",  "alien_material:alien_diamond",  "alien_material:alien_diamond",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_mese_block",
    recipe = {
        {"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
				{"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
				{"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_mese 9",
    recipe = {
        {"alien_material:alien_mese_block"},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_mese_block",
    recipe = {
        {"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
				{"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
				{"alien_material:alien_mese",  "alien_material:alien_mese",  "alien_material:alien_mese",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_pickaxe",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"",  "alien_material:alien_mese_fragment",  "",},
				{"",  "alien_material:alien_mese_fragment",  "",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_axe",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_mese_fragment",  "alien_material:alien_ingot",},
				{"alien_material:alien_mese_fragment",  "",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_sword",
    recipe = {
        {"alien_material:alien_ingot",},
				{"alien_material:alien_ingot",},
				{"alien_material:alien_mese_fragment",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_spade",
    recipe = {
        {"alien_material:alien_ingot",},
				{"alien_material:alien_mese_fragment",},
				{"alien_material:alien_mese_fragment",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_ingot",
    recipe = {
        {"alien_material:alien_mese",  "alien_material:alien_diamond",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_block",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_ingot",
    recipe = {
        {"alien_material:alien_block",},
        }
})

-- armor

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_chestplate",
    recipe = {
        {"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_leggings",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_shield",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"",  "alien_material:alien_ingot",  "",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_helmet",
    recipe = {
        {"alien_material:alien_ingot",  "alien_material:alien_ingot",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
    }
})

minetest.register_craft({
    type = "shaped",
    output = "alien_material:alien_boots",
    recipe = {
        {"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
				{"alien_material:alien_ingot",  "",  "alien_material:alien_ingot",},
    }
})

-- Fuel
minetest.register_craft({
  type = "fuel",
  recipe = "alien_material:alien_ingot",
  burntime = 1024,
})
