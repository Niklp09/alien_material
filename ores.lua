minetest.register_ore({
    ore_type       = "scatter",
    ore            = "alien_material:alien_mese_ore",
    wherein        = "default:stone",
    clust_scarcity = 18*18*18,
    clust_num_ores = 4,
    clust_size     = 2,
    height_min     = -31000,
    height_max     = -2048,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "alien_material:alien_diamond_ore",
    wherein        = "default:stone",
    clust_scarcity = 18*18*18,
    clust_num_ores = 4,
    clust_size     = 2,
    height_min     = -31000,
    height_max     = -2048,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "alien_material:alien_mese_block",
    wherein        = "default:stone",
    clust_scarcity = 18*19*19,
    clust_num_ores = 4,
    clust_size     = 2,
    height_min     = -31000,
    height_max     = -4096,
})
